package de.isium.gol;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class PlayGroundTest 
{
    @Test
    public void createInvalid()
    {
        assertThrows(RuntimeException.class, () -> {
            new PlayGround(3, 2);
        });
    }

    @Test
    public void createInvalidFromArray()
    {
        assertThrows(RuntimeException.class, () -> {
            new PlayGround(new boolean[2][3]);
        });
    }

    @Test
    public void inPlayground() {
        PlayGround pg = new PlayGround();

        assertTrue(pg.isInPlayGround(0, 0));
        assertTrue(pg.isInPlayGround(2, 2));
        assertTrue(pg.isInPlayGround(1, 1));
        assertFalse(pg.isInPlayGround(3, 0));
        assertFalse(pg.isInPlayGround(1, -1));
        assertFalse(pg.isInPlayGround(-1, 4));

    }
}
