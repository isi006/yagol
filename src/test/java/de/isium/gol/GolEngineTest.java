package de.isium.gol;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class GolEngineTest 
{
    private GolEngine testee;

    @BeforeEach
    public void setup() {
        testee = new GolEngine();
    }

    @Test
    public void countNeightbours()
    {
        PlayGround pg = new PlayGround();
        pg.setCell(0, 0, true);

        assertEquals(0, testee.countNeighbours(pg, 0, 0));
        assertEquals(1, testee.countNeighbours(pg, 0, 1));
        assertEquals(0, testee.countNeighbours(pg, 0, 2));
        assertEquals(1, testee.countNeighbours(pg, 1, 0));
        assertEquals(1, testee.countNeighbours(pg, 1, 1));
        assertEquals(0, testee.countNeighbours(pg, 1, 2));
        assertEquals(0, testee.countNeighbours(pg, 2, 0));
        assertEquals(0, testee.countNeighbours(pg, 2, 1));
        assertEquals(0, testee.countNeighbours(pg, 2, 2));
    }

    @Test
    public void newBorn() {
        final PlayGround pg = new PlayGround();
        pg.setCell(0, 0, true);
        pg.setCell(0, 1, true);
        pg.setCell(0, 2, true);

        final PlayGround newGen = testee.processGeneration(pg);

        assertEquals(true, newGen.getCell( 1, 1));

    }

    @Test
    public void dieNotEnough() {
        final PlayGround pg = new PlayGround();
        pg.setCell(0, 1, true);
        pg.setCell(1, 1, true);

        final PlayGround newGen = testee.processGeneration(pg);

        assertEquals(false, newGen.getCell( 0, 1));
        assertEquals(false, newGen.getCell( 1, 1));

    }

    @Test
    public void dieToMuch() {

        final PlayGround pg = new PlayGround();
        
        pg.setCell(0, 0, true);
        pg.setCell(0, 1, true);
        pg.setCell(0, 2, true);
        pg.setCell(1, 0, true);

        pg.setCell(1, 1, true);

        final PlayGround newGen = testee.processGeneration(pg);

        assertEquals(false, newGen.getCell( 1, 1));

    }

    @Test
    public void allGenerations() {

        final String startGen =
            // GEN 0
            "-------\n" +
            "-------\n" +
            "---*---\n" +
            "----*--\n" +
            "--***--\n" +
            "-------\n" +
            "-------\n";
        

        final String[] expectedGenerations = {
            // GEN 1
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "--*-*--\n" +
            "---**--\n" +
            "---*---\n" +
            "-------\n",
            // GEN 2
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "----*--\n" +
            "--*-*--\n" +
            "---**--\n" +
            "-------\n",
            // GEN 3
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "---*---\n" +
            "----**-\n" +
            "---**--\n" +
            "-------\n",
            // GEN 4
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "----*--\n" +
            "-----*-\n" +
            "---***-\n" +
            "-------\n",
            // GEN 5
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "---*-*-\n" +
            "----**-\n" +
            "----*--\n",
            // GEN 6
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "-----*-\n" +
            "---*-*-\n" +
            "----**-\n",
            // GEN 7
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "----*--\n" +
            "-----**\n" +
            "----**-\n"
        };


        PlayGround pg = new PlayGround(startGen);

        for (var expGen : expectedGenerations) {
            pg = testee.processGeneration(pg);
            assertEquals(expGen, pg.toString());
        }

    }
    @Test
    public void gen3() {

        final String startGen =
            // GEN 2
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "----*--\n" +
            "--*-*--\n" +
            "---**--\n" +
            "-------\n";
        

        final String[] expectedGenerations = {
            // GEN 3
            "-------\n" +
            "-------\n" +
            "-------\n" +
            "---*---\n" +
            "----**-\n" +
            "---**--\n" +
            "-------\n"
        };


        PlayGround pg = new PlayGround(startGen);

        for (var expGen : expectedGenerations) {
            pg = testee.processGeneration(pg);
            assertEquals(expGen, pg.toString());
        }

    }


}
