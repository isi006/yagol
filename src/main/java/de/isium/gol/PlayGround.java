package de.isium.gol;

public class PlayGround {

  private final static int MIN_ROWS = 3;
  private final static int MIN_COLS = 3;

  private final boolean[][] cells;

  public PlayGround(final boolean[][] cells) {
    validateGrid(cells.length, cells[0].length);
    this.cells = cells;
  }

  public void validateGrid(int rows, int cols) {
    if(rows < MIN_ROWS) {
      throw new RuntimeException("not enough rows, please choose at least " + MIN_ROWS);
    }
    if(cols < MIN_COLS) {
      throw new RuntimeException("not enough cols, please choose at least " + MIN_COLS);
    }
  }

  public PlayGround(final int rows, final int cols) {
    validateGrid(rows, cols);
    this.cells = new boolean[rows][cols];
  }

  public PlayGround(final String cellStr) {
    this(stringToCells(cellStr));
  }

  public PlayGround() {
    this(MIN_ROWS, MIN_COLS);
  }

  public boolean isInPlayGround(int row, int col) {
    return (row >= 0 && row < cells.length) && (col >= 0 && col < cells[0].length); 
  }

  public void validatePos(int row, int col) {
    if(!isInPlayGround(row, col)) {
      throw new RuntimeException("Out of playground Exception: " + row + ", " + col);
    }
  }

  public boolean[][] getCells() {
    return cells;
  }
  
  public int getRowCount() {
    return cells.length;
  }
  public int getColCount() {
    return cells[0].length;
  }

  public boolean getCell(int row, int col) {
    validatePos(row, col);
    return cells[row][col];
  }

  public void setCell(int row, int col, boolean value) {
    validatePos(row, col);
    cells[row][col] = value;   
  }

  public String toString() {
    StringBuffer result = new StringBuffer();

    for (var row : cells) {
      for(var cell : row) {
        result.append(cell ? "*" : "-");
      }
      result.append("\n");
    }

    return result.toString();
  }


  private static boolean[][] stringToCells(final String cellStr) {
    final String[] rows = cellStr.split("\\n");
    final var rowCount = rows.length;
    final var colCount = rows[0].length();
    
    boolean[][] result = new boolean[rowCount][colCount];
    
    for (var row = 0; row < rowCount; row++) {
        final char[] rowChars = rows[row].toCharArray();
        for (var col = 0; col < colCount; col++ ) {
            result[row][col] = (rowChars[col] == '*'); 
        }
    }

    return result;
  }


}