package de.isium.gol;

public class GolEngine {

  private final static int[][] OFFSETS = { 
    { -1, -1 }, { -1, 0 }, { -1, 1 },
    {  0, -1 },            {  0, 1 },
    {  1, -1 }, {  1, 0 }, {  1, 1 }
  };

  public int countNeighbours(final PlayGround pg, final int row, final int col) {
    
    var nb = 0;

    for (final int[] offset : OFFSETS) {
      final int r = row + offset[0];
      final int c = col + offset[1];
      if(pg.isInPlayGround(r, c)) {
        if(pg.getCell(r, c)) {
          nb++;
        }
      }
    };
    
    return nb;
  }

  public PlayGround processGeneration(final PlayGround pg) {
 
    final PlayGround newGen = new PlayGround(pg.getRowCount(), pg.getColCount());

    for (var row = 0; row < pg.getRowCount(); row++) {
      for(var col = 0; col < pg.getColCount(); col++) {
        final var live = pg.getCell(row, col);
        var newVal = live;
        final var nb = countNeighbours(pg, row, col);
        if (!live && nb == 3) {
          newVal = true;
        }
        else if (live && (nb < 2 || nb > 3)) {
          newVal = false;
        }
        newGen.setCell(row, col, newVal);
      }
    }

    return newGen;
  }
  
}