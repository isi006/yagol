package de.isium.gol;

/**
 * Hello world!
 *
 */
public class App 
{

    private static final int ITERATIONS = 100;

    private static final String START_10CELLROW = 
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "-----------**********-----------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n" +
    "--------------------------------\n";



    private static final String START_GLIDER = 
        "-------\n" +
        "-------\n" +
        "---*---\n" +
        "----*--\n" +
        "--***--\n" +
        "-------\n" +
        "-------\n";

    public static void main( String[] args ) throws InterruptedException
    {
        System.out.println( "GAME OF LIVE" );

        GolEngine engine = new GolEngine();

        PlayGround pg = new PlayGround(START_10CELLROW);

        System.out.println(pg.toString());

        for (var i = 1; i <= ITERATIONS; i++) {
            pg = engine.processGeneration(pg);
            System.out.println("Generation: " + i);
            System.out.println(pg.toString());
            Thread.sleep(250);
        }

    }
}
